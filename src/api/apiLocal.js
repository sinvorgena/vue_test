const localStorageName = "cryptonomicon"

export const saveToLocal = (items) => {
    localStorage.setItem(localStorageName, items)
}
export const getFromLocal = () => {
    return localStorage.getItem(localStorageName)
}