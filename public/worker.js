let ports = []
self.addEventListener("connect", e => {
    const port = e.ports[0]
    ports.push(port)
    if (ports.length === 1) {
        port.postMessage({original: true});
    } else {
        port.postMessage({original: false});
    }
    e.source.addEventListener("message", ev => {
        ports.forEach((port, index) => {
            if (index === 0) {
                port.postMessage({tickets: ev.data.tickets});
                return
            }
            port.postMessage({tickets: ev.data.tickets});
        })
    }, false);
    e.source.start();
}, false);